#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


char encrypt(char letter, char key){
    unsigned char cipher_letter;
    cipher_letter = letter + (key - 'a');
    if (cipher_letter > 122){
        cipher_letter -= 'z' - 'a' + 1;
    }
    return (char)cipher_letter;
}

char decrypt(char letter, char key){
    char cipher_letter = letter - (key - 'a');
    if ( letter < 97 || letter > 122 ){
        return letter;
    }
    if (cipher_letter < 97){
        cipher_letter += 'z' - 'a' + 1;
    }
    return cipher_letter;
}

char* cipher(char *text, char *keyword){
    char letter;
    char kletter;
    char encrypted;
    int idx = 0;
    int kidx = 0;
    while (text[idx] != '\0'){
        letter = text[idx];
        letter = (char)tolower((int)letter);
        if ( letter < 97 || letter > 122 ){
            idx++;
            continue;
        }
        kletter = keyword[kidx++];
        encrypted = encrypt(letter, kletter);
        text[idx++] = encrypted;
        if (keyword[kidx] == '\0'){
            kidx = 0;
        }
    }
    text[idx] = '\0';
    return text;
}

int main(int argc, char *argv[]){
    char a[26] = "I love CS341! It's great!";
    printf(">>>%s\n", cipher(a, argv[2]));
    return 0;
}
